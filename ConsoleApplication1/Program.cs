﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace dotnet_lab2
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1 - żyje, 0 - nie żyje
            int n = 4; //rozmiar planszy
            int[,] array = new int[,] { { 0, 0, 0, 0, 0, 0 }, { 0, 1, 1, 1, 0, 0 }, { 0, 0, 0, 0, 1, 0 }, { 0, 0, 0, 1, 0, 0 }, { 0, 0, 0, 0, 0, 0 }, { 0, 0, 0, 0, 0, 0 } };
            int[,] array_new = new int[n + 2, n + 2];
            int i, j, k, l, liczba_sasiadow;
            ConsoleKeyInfo key;

            do
            {
                Console.Clear();
                //wyswietlenie stanu planszy
                for (i = 1; i < n + 1; i++)
                {
                    for (j = 1; j < n + 1; j++)
                        Console.Write(array[i, j]);
                    Console.Write("\n");
                }

                Console.Write("\nDowolny klawisz - kontynuuj, ESC - zakończ.");

                for (i = 1; i < n + 1; i++) //przejscie po planszy
                {
                    for (j = 1; j < n + 1; j++)
                    {
                        liczba_sasiadow = 0;
                        for (k = i - 1; k <= i + 1; k++) //przejscie po sasiadach
                            for (l = j - 1; l <= j + 1; l++)
                                liczba_sasiadow += array[k, l];
                        liczba_sasiadow -= array[i, j];
                        //zaktualizowanie zawartosci pola
                        if (liczba_sasiadow < 2) array_new[i, j] = 0;
                        if (liczba_sasiadow > 3) array_new[i, j] = 0;
                        if (liczba_sasiadow == 3) array_new[i, j] = 1;
                        if (liczba_sasiadow == 2 && array[i, j] == 1) array_new[i, j] = 1;
                    }
                    Console.Write("\n");
                }

                Array.Copy(array_new, array, array_new.Length);
                key = System.Console.ReadKey();

            } while (key.Key != ConsoleKey.Escape);
        }
    }
}